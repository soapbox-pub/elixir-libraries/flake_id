defmodule FlakeIdTest do
  use ExUnit.Case, async: true
  doctest FlakeId

  @flake_string "9n2ciuz1wdesFnrGJU"
  @flake_binary <<0, 0, 1, 109, 67, 124, 251, 125, 95, 28, 30, 59, 36, 42, 0, 0>>
  @flake_integer 28_939_165_907_792_829_150_732_718_047_232

  test "flake_id?/1" do
    assert FlakeId.flake_id?(@flake_string)
    refute FlakeId.flake_id?("http://example.com/activities/3ebbadd1-eb14-4e20-8118-b6f79c0c7b0b")
    refute FlakeId.flake_id?("#cofe")
  end

  test "get/0" do
    flake = FlakeId.get()

    assert String.valid?(flake)
    assert FlakeId.flake_id?(flake)
  end

  describe "to_string/1" do
    test "with binary" do
      assert FlakeId.to_string(@flake_binary) == @flake_string

      bin = <<1::integer-size(64), 2::integer-size(48), 3::integer-size(16)>>
      assert FlakeId.to_string(bin) == "LygHa16ApeN"
    end

    test "does nothing with other types" do
      assert FlakeId.to_string("cofe") == "cofe"
      assert FlakeId.to_string(42) == 42
    end
  end

  describe "from_string/1" do
    test "with a flake string" do
      assert FlakeId.from_string(@flake_string) == @flake_binary
    end

    test "with a flake binary" do
      assert FlakeId.from_string(@flake_binary) == @flake_binary
    end

    test "with a non flake string" do
      assert FlakeId.from_string("cofe") == nil
    end
  end

  test "to_integer/1" do
    assert FlakeId.to_integer(@flake_binary) == @flake_integer
  end

  test "from_integer/1" do
    assert FlakeId.from_integer(@flake_integer) == @flake_binary
  end
end
