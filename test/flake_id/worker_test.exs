defmodule FlakeId.WorkerTest do
  use ExUnit.Case, async: true

  alias FlakeId.Worker

  test "get/1" do
    flake = Worker.get()

    assert is_binary(flake)
    assert <<_::integer-size(128)>> = flake
  end

  describe "get/2" do
    test "increment `:sq` when the calling time is the same as the state time" do
      time = Worker.time()
      node = Worker.worker_id()
      state = %Worker{time: time, node: node, sq: 0}

      assert {<<_::integer-size(128)>>, %Worker{node: ^node, time: ^time, sq: 1}} =
               Worker.get(time, state)
    end

    test "reset `:sq` when the times are different" do
      time = Worker.time()
      node = Worker.worker_id()
      state = %Worker{time: time - 1, node: node, sq: 42}

      assert {<<_::integer-size(128)>>, %Worker{time: ^time, sq: 0}} = Worker.get(time, state)
    end

    test "wrror when clock is running backwards" do
      time = Worker.time()
      state = %Worker{time: time + 1}

      assert Worker.get(time, state) == {:error, :clock_running_backwards}
    end
  end
end
