defmodule FlakeId.Ecto.TypeTest do
  use ExUnit.Case, async: true

  alias FlakeId.Ecto.Type

  @flake_string "9n2ciuz1wdesFnrGJU"
  @flake_binary <<0, 0, 1, 109, 67, 124, 251, 125, 95, 28, 30, 59, 36, 42, 0, 0>>

  test "cast/1" do
    assert Type.cast(@flake_binary) == {:ok, @flake_string}
    assert Type.cast(@flake_string) == {:ok, @flake_string}
  end

  test "load/1" do
    assert Type.load(@flake_binary) == {:ok, @flake_string}
    assert Type.load(@flake_string) == {:ok, @flake_string}
  end

  test "dump/1" do
    assert Type.dump(@flake_string) == {:ok, @flake_binary}
    assert Type.dump(@flake_binary) == {:ok, @flake_binary}
  end

  test "equal?/2" do
    assert Type.equal?(@flake_binary, @flake_binary) == true
  end

  test "autogenerate/0" do
    flake = Type.autogenerate()

    assert String.valid?(flake)
    assert FlakeId.flake_id?(flake)
  end
end
