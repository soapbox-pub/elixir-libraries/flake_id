defmodule FlakeId.Ecto.CompatTypeTest do
  use ExUnit.Case, async: true

  alias FlakeId.Ecto.CompatType

  @flake_string "9n2ciuz1wdesFnrGJU"
  @flake_binary <<0, 0, 1, 109, 67, 124, 251, 125, 95, 28, 30, 59, 36, 42, 0, 0>>
  @fake_flake <<0::integer-size(64), 42::integer-size(64)>>

  test "cast/1" do
    assert CompatType.cast(@flake_binary) == {:ok, @flake_string}
    assert CompatType.cast(@flake_string) == {:ok, @flake_string}
  end

  test "load/1" do
    assert CompatType.load(@flake_binary) == {:ok, @flake_string}
    assert CompatType.load(@flake_string) == {:ok, @flake_string}
  end

  test "dump/1" do
    assert CompatType.dump(@flake_string) == {:ok, @flake_binary}
    assert CompatType.dump(@flake_binary) == {:ok, @flake_binary}
  end

  test "equal?/2" do
    assert CompatType.equal?(@flake_binary, @flake_binary) == true
  end

  test "autogenerate/0" do
    flake = CompatType.autogenerate()

    assert String.valid?(flake)
    assert FlakeId.flake_id?(flake)
  end

  describe "fake flakes (compatibility with older serial integers)" do
    test "with an integer" do
      assert CompatType.dump(42) == {:ok, @fake_flake}
    end

    test "with an integer as string" do
      assert CompatType.dump("42") == {:ok, @fake_flake}
    end

    test "zero or -1 is a null flake" do
      null_flake = <<0::integer-size(128)>>
      assert CompatType.dump(0) == {:ok, null_flake}
      assert CompatType.dump(-1) == {:ok, null_flake}
      assert CompatType.dump("0") == {:ok, null_flake}
      assert CompatType.dump("-1") == {:ok, null_flake}
    end

    test "cast" do
      assert CompatType.cast(@fake_flake) == {:ok, "42"}
    end
  end
end
