# FlakeId: Decentralized, k-ordered ID generation service
# Copyright © 2017-2019 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: LGPL-3.0-only

defmodule FlakeId.Ecto.Type do
  @moduledoc """
  Provides a type for Ecto usage.

  The underlying data type should be an `:uuid`.

  ## Migration Example

      create table(:posts, primary_key: false) do
        add :id, :uuid, primary_key: true
        add :body, :text
      end

  ## Schema Example

      @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

      schema "posts" do
        add :body, :string
      end

  """
  @behaviour Ecto.Type
  def embed_as(_), do: :self
  def equal?(term1, term2), do: term1 == term2

  def type, do: :uuid

  defdelegate autogenerate, to: FlakeId, as: :get

  def cast(value) do
    {:ok, FlakeId.to_string(value)}
  end

  def load(value) do
    {:ok, FlakeId.to_string(value)}
  end

  def dump(value) do
    {:ok, FlakeId.from_string(value)}
  end
end
