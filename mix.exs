defmodule FlakeId.MixProject do
  use Mix.Project

  def project do
    [
      app: :flake_id,
      version: "0.1.0",
      description: "Decentralized, k-ordered ID generation service",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),

      # Docs
      name: "FlakeId",
      source_url: "https://git.pleroma.social/pleroma/flake_id",
      homepage_url: "https://git.pleroma.social/pleroma/flake_id",
      docs: [
        main: "readme",
        extras: ["README.md"],
        source_url_pattern:
          "https://git.pleroma.social/pleroma/flake_id/blob/master/%{path}#L%{line}"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {FlakeId.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:ecto, ">= 2.0.0", optional: true},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:base62, "~> 1.2"}
    ]
  end

  defp package do
    [
      licenses: ["LGPLv3"],
      links: %{"GitLab" => "https://git.pleroma.social/pleroma/flake_id"}
    ]
  end
end
