# ❄ FlakeId

> Decentralized, k-ordered ID generation service

## Installation

Add `flake_id` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:flake_id, "~> 0.1.0"}
  ]
end
```

## Usage

```elixir
iex> FlakeId.get()
"9n3171dJZpdD77K3DU"
```

See [https://hexdocs.pm/flake_id](https://hexdocs.pm/flake_id) for the complete documentation.

### With Ecto

It is possible to use `FlakeId` with [`Ecto`](https://github.com/elixir-ecto/ecto/). See [`FlakeId.Ecto.Type`](https://hexdocs.pm/flake_id/FlakeId.Ecto.Type.html) documentation for detailed instructions.

## Prior Art

* [flaky](https://github.com/nirvana/flaky), released under the terms of the Truly Free License,
* [Flake](https://github.com/boundary/flake), Copyright 2012, Boundary, Apache License, Version 2.0

## Copyright and License

Copyright © 2017-2019 [Pleroma Authors](https://pleroma.social/)

FlakeId source code is licensed under the GNU LGPLv3 License.
